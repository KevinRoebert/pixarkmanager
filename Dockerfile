FROM debian:buster-slim
LABEL maintainer="Kevin Röbert <dev@roebert.eu>"

ARG DEBIAN_FRONTEND=noninteractive
ARG USER=pixark
ARG GROUP=pixark
ARG PIXARKDIR=/pixark
ARG STEAMCMDDIR=/home/steam/steamcmd
ARG WINEARCH='win64'

RUN dpkg --add-architecture i386 && \
    apt-get update && apt-get install -y --no-install-recommends --no-install-suggests \
        lib32stdc++6 \
        lib32gcc1 \
        wget \
        xvfb \
        ca-certificates \
        wine \
        wine32 \
        wine64 \
        winbind && \
    wine wineboot --init && \
    groupadd $GROUP && \
    useradd -g $GROUP -d $PIXARKDIR $USER && \
    mkdir -p ${STEAMCMDDIR} && \
    mkdir -p ${PIXARKDIR} && \
    chown $USER:$GROUPE $PIXARKDIR && \
    cd ${STEAMCMDDIR} && \
    wget -qO- 'https://steamcdn-a.akamaihd.net/client/installer/steamcmd_linux.tar.gz' | tar zxf - && \
    apt-get remove --purge -y wget && \
    apt-get clean autoclean && \
    apt-get autoremove -y && \
    rm -rf /var/lib/apt/lists/* /root/.steam/logs/* /tmp/*

WORKDIR $STEAMCMDDIR

RUN ./steamcmd.sh +quit && \
    rm -rf /root/.steam/logs/* /tmp/*

RUN ./steamcmd.sh +@sSteamCmdForcePlatformType windows +login anonymous +force_install_dir $PIXARKDIR +app_update 824360 validate +quit && \
    rm -rf /root/.steam/logs/* /tmp/*

COPY ./entrypoint.sh /pixark/entrypoint.sh
RUN chmod +x /pixark/entrypoint.sh

ENV PIXARKDIR=$PIXARKDIR
ENV USER=$USER
ENV GROUP=$GROUP
ENV DATA=$PIXARKDIR/ShooterGame/Saved
ENV WINEPREFIX=$PIXARKDIR
ENV DISPLAY :0

VOLUME ["${DATA}"]
USER $USER

EXPOSE 27015-27018/tcp
EXPOSE 27016-27018/udp

ENTRYPOINT ["/pixark/entrypoint.sh"]
