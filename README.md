# About
This repo offers two different ways to set up a PixARK server. Either using a Docker image (including a preconfigured docker-compose file) or using an installation script.

# Docker
Just copy the docker-compose file and adjust it if necessary.

The ready-made docker-compose file needs a folder named `savedGames` in the same folder. In this folder the settings and saved games are saved, so that after an update they are still available.

## Minimum settings
In the `.env` file simple settings like server name or maximum number of players can be changed.

If you want to add a start parameter to the server, you have to do this in the docker-compose under 'command'.

## Start
To start simply enter 'docker-compose up -d' in the destination folder.

## Stop
To finish, enter `docker-compose down`.

## Update
Bash into the docker image with `docker exec -it "pixark" /bin/bash`, then run the steam update with: `./steamcmd.sh +@sSteamCmdForcePlatformType windows +login anonymous +force_install_dir /pixark +app_update 824360 validate +quit`

# Script
This script/manager will help inexperienced users to installing and manage a PixARK server on Linux.

This script/manager has been successfully tested on Debian 9. For Ubuntu 18.04 LTS you've to install wine manually and update all wine64 calls to wine.

## Installation

`wget https://gitlab.com/KevinRoebert/pixarkmanager/raw/master/pixarkmanager.sh -v -O pixarkmanager.sh && sudo chmod +x pixarkmanager.sh && sudo ./pixarkmanager.sh --install`

## Usage
Type in the console `pixarkmanager` followed by a command.

**This commands are available:**

`--help` or `-h`: Displays the help page.

`--install` or `-i`: Install the pixarkmanager and the pixARK server with all dependencies.

`--update` or `-u`: Stops and update the pixARK server. Before run this command, please make a saveworld in your ARK ingame Admin console.

`--start` or `-s`: Starts the pixARK server.

`--status`: Shows the pixARK server output.

`--restart` or `-r`: Restarts the pixARK server. Before run this command, please make a saveworld in your ARK ingame Admin console.

`--stop`: Stops the pixARK server. Before run this command, please make a saveworld in your ARK ingame Admin console.

`--reconfigure`: You can set new configurations for your pixARK server. Before run this command, please make a saveworld in your ARK ingame Admin console.

`--self-upgrade`: Upgrades the pixarkmanger.


# License [![CC BY-NC-SA 4.0](https://img.shields.io/badge/license-CC%20BY--NC--SA%204.0-green.svg)](https://creativecommons.org/licenses/by-nc-sa/4.0/)
(c) 2018 Kevin Röbert
