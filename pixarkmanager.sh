#!/bin/bash

# (c) 2018 Kevin Röbert | CC BY-NC-SA 4.0

# some vars
INSTALL_DIR='/etc/pixarkmanager'
SCRIPT_URL='https://gitlab.com/KevinRoebert/pixarkmanager/raw/master/pixarkmanager.sh'
STEAM_DIR='/usr/games/steamcmd'
PIXARK_DIR='/home/pixark'
VERSION=1.0

# some colors
NC='\033[0m' # No Color
RED='\033[0;31m'
GREEN='\033[0;32m'
BLUE='\033[0;34m'

# check permissions
if [ "$EUID" -ne 0 ]
     then echo -e "${RED}Please run this script as root${NC}"
     exit
fi

# upgrade the pixarkmanager script
upgrade_pixarkmanager()
{
    mkdir -p "$INSTALL_DIR"
    cd "$INSTALL_DIR"
    wget -q "$SCRIPT_URL" -O pixarkmanager.sh
    chmod +x pixarkmanager.sh
}

# install pixarkmanager
install_pixarkmanager()
{
    upgrade_pixarkmanager

    # create shortcut for pixarkmanager
    echo -e "#!/bin/sh\n/etc/pixarkmanager/pixarkmanager.sh \"\$@\"" > '/usr/bin/pixarkmanager'
    chmod +x /usr/bin/pixarkmanager
}

# install or update pixARK server
update_this()
{
    stop_this
    cd "$STEAM_DIR"
    # download or update pixARK server
    ./steamcmd.sh +@sSteamCmdForcePlatformType windows +login anonymous +force_install_dir "$PIXARK_DIR" +app_update 824360 validate +quit
}

# create start helper script
create_scripts()
{
    # create pixARK start script
    cd "$INSTALL_DIR"
    echo -e "#!/bin/bash\nwine64 $PIXARK_DIR/ShooterGame/Binaries/Win64/PixARKServer.exe \"CubeWorld_Light?listen?MaxPlayers=$maxPlayers?SessionName=${servername}?Port=${port}?QueryPort=${queryPort}?RCONPort=${RCONPort}?ServerAdminPassword=${password}?CULTUREFORCOOKING=en\" ${battleEye} -CubePort=${CubePort} -cubeworld=world -nosteamclient -NoHangDetection -game -server -log" > HelperStartPixARK.sh
    chmod +x HelperStartPixARK.sh
}

# starts the pixARK server
start_this()
{
    cd "$INSTALL_DIR"
    if ! screen -list | grep -q "PixARK"; then
        screen -A -m -d -S PixARK ./HelperStartPixARK.sh
        echo -e "${GREEN}The pixARK server was started. The first start can take up to 20 minutes.${NC}\n "
        echo -e "You can see the current server output with the following command: ${BLUE}pixarkmanager --status${NC}\n"
    else
        echo -e "${RED}A pixARK server is already running!${NC}"
    fi
}

# stop the pixARK server
stop_this()
{
    echo -e "${GREEN}Try to stopp the pixARK server...${NC}\n"

    cd "$INSTALL_DIR"

    SESSION_NAME='PixARK'
    screen -ls "$SESSION_NAME" | (
        IFS=$(printf '\t');
        sed "s/^$IFS//" |
        while read -r name stuff; do
            screen -S "$name" -X quit
        done
    )

    sleep 3

    if ! screen -list | grep -q "PixARK"; then
        echo -e "${RED}First attempt failed. Trying force close.${NC}"
        pkill screen
    fi

    echo -e "${GREEN}Server was stopped.${NC}"
}

# goes into the pixARK screen
status_this()
{
    if ! screen -list | grep -q "PixARK"; then
        echo -e "${RED}No pixARK server is running!${NC}"
    else
        screen -r PixARK
    fi
}

# restart the pixARK server
restart_this()
{
    stop_this
    start_this
}

# configure the server
set_conf()
{
    # ask user for pixARK server configs
    echo -e "${GREEN}Please enter your data for the pixARK server configuration.${NC}\n"
    echo "Servername: "
    read servername

    while [[ -z "$servername" ]]
    do
            echo -e "${RED}You must enter a servername:${NC} "
            read servername
    done

    echo "Max. players: "
    read maxPlayers

    while [[ -z "$maxPlayers" ]]
    do
    	echo -e "${RED}You must enter max. players:${NC} "
    	read maxPlayers
    done

    echo "Port (Default: 27015): "
    read port

    if [[ -z "$port" ]]; then
    	port=27015
    fi

    echo "QueryPort (Default: 27016): "
    read queryPort

    if [[ -z "$queryPort" ]]; then
    	queryPort=27016
    fi

    echo "RCONPort (Default: 27017): "
    read RCONPort

    if [[ -z "$RCONPort" ]]; then
    	RCONPort=27017
    fi

    echo "CubePort (Default: 27018): "
    read CubePort

    if [[ -z "$CubePort" ]]; then
    	CubePort=27018
    fi

    echo "Server Admin password: "
    read password

    echo "Would you like to activate BattleEye? (yes or no): "
    read battleEye

    while [[ "$battleEye" != "yes" ]] && [[ "$battleEye" != "no" ]]
    do
    	echo "Please input yes or no"
    	echo "Would you like to activate BattleEye? (yes or no): "
    	read battleEye
    done

    if [ "$battleEye" == "yes" ]; then
    	battleEye=""
    else
    	battleEye="-NoBattlEye"
    fi
}

# set firewall rules
set_firewall_rules()
{
    iptables -A INPUT -p tcp -m tcp --dport $port:$port -j ACCEPT
    iptables -A INPUT -p tcp -m tcp --dport $queryPort:$queryPort -j ACCEPT
    iptables -A INPUT -p tcp -m tcp --dport $RCONPort:$RCONPort -j ACCEPT
    iptables -A INPUT -p tcp -m tcp --dport $CubePort:$CubePort -j ACCEPT
}

# reconfigures the pixARK server
reconfigure_this()
{
    stop_this
    set_conf
    create_scripts
    set_firewall_rules
}

## install necessary packages, pixARK server and configure wine
install_all()
{
    # install the pixarkmanager
    install_pixarkmanager

    # add 64-bit OS architecture for wine64
    dpkg --add-architecture i386

    # update package list
    apt-get update

    # install steamcmd
    apt-get install lib32gcc1 -y
    mkdir -p "$STEAM_DIR"
    cd "$STEAM_DIR"
    wget https://steamcdn-a.akamaihd.net/client/installer/steamcmd_linux.tar.gz
    tar -xvzf steamcmd_linux.tar.gz

    # install screen
    apt-get install screen -y

    # install wine
    apt-get install wine -y
    apt-get install wine64 -y
    apt-get install winbind -y

    # update system
    apt-get update

    # set wine to 64-bit architecture
    export WINEARCH='win64'
    wine wineboot --init

    update_this
    reconfigure_this
}

# show help page
helpPage()
{
    echo -e "${GREEN}Version ${VERSION}${NC}\n"
    echo -e "--help or -h: This help page.\n"
    echo -e "--install or -i: Install the pixarkmanager and the pixARK server with all dependencies.\n"
    echo -e "--update or -u: Stops and update the pixARK server. ${RED}Before run this command, please make a ${NC}${BLUE}saveworld${NC} ${RED}in your ARK ingame Admin console.${NC}\n"
    echo -e "--start or -s: Starts the pixARK server.\n"
    echo -e "--status: Shows the pixARK server output.\n"
    echo -e "--restart or -r: Restarts the pixARK server. ${RED}Before run this command, please make a ${NC}${BLUE}saveworld${NC} ${RED}in your ARK ingame Admin console.${NC}\n"
    echo -e "--stop: Stops the pixARK server. ${RED}Before run this command, please make a ${NC}${BLUE}saveworld${NC} ${RED}in your ARK ingame Admin console.${NC}\n"
    echo -e "--reconfigure: You can set new configurations for your pixARK server. ${RED}Before run this command, please make a ${NC}${BLUE}saveworld${NC} ${RED}in your ARK ingame Admin console.${NC}\n"
    echo -e "--self-upgrade: Upgrades the pixarkmanger.\n"
}

# check for arguments
while [ $# -gt 0 ]
do
    case $1 in
        --install | -i)
            install_all
            exit
            ;;
        --self-upgrade)
            upgrade_pixarkmanager
            exit
            ;;
        --update | -u)
            update_this
            exit
            ;;
        --start | -s)
            start_this
            exit
            ;;
        --stop)
            stop_this
            exit
            ;;
        --reconfigure)
            reconfigure_this
            exit
            ;;
        --restart | -r)
            restart_this
            exit
            ;;
        --status)
            status_this
            exit
            ;;
        --help | *)
            helpPage
            exit
            ;;
    esac
    shift
done

helpPage
